﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage_Test.master" CodeFile="ForgotPassword.aspx.vb" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <br />
    <div class="container card bg-primary text-white h-100">
        <div class="card-header">
            <div class="col-md-12">
                <h3 style="text-align: center;">Forgot your password Or Expired?</h3>
                <hr />
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                <div class="row">
                    <%-- <div class="col-md-3">
                        <label>User ID</label>
                        <asp:TextBox ID="txt_UserID" placeholder="Enter User ID" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <label>Registered Email</label>
                        <asp:TextBox ID="txt_EmailID" CssClass="form-control" placeholder="Enter Registered Email" runat="server"></asp:TextBox>
                    </div>--%>
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <label>Registered Mobile No</label>
                        <asp:TextBox ID="txt_MobileNo" class="form-control" placeholder="Enter Registered Mobile No" runat="server" MaxLength="10"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <label>&nbsp;</label>
                        <asp:Button ID="Button1" runat="server" Text="Get Password" CssClass="btn btn-danger" BorderColor="#161946" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" Style="margin-top: 24px;" />
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br />
                <br />
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
</asp:Content>
