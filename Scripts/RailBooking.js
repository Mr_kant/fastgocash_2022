﻿var RailHandler;
$(document).ready(function () {
    RailHandler = new RailHelper();
    RailHandler.BindEvents();
});
var RailHelper = function () {
    this.txtAgencyName = $("#txtAgencyName");
    this.hidtxtAgencyName = $("#hidtxtAgencyName");
    this.FormID = $("#ctl00_ContentPlaceHolder1_FromID");
    this.ToID = $("#ctl00_ContentPlaceHolder1_ToID");
    this.TxtTravelFrom = $("#ctl00_ContentPlaceHolder1_TxtTravelFrom");
    this.TxtTravelTo = $("#ctl00_ContentPlaceHolder1_TxtTravelTo");
    this.TxtFrom = $("#ctl00_ContentPlaceHolder1_TxtFrom");
    this.TxtTO = $("#ctl00_ContentPlaceHolder1_TxtTO");
    var dates = new Date();
}

RailHelper.prototype.BindEvents = function () {
    // 
    var h = this;
    if (h.txtAgencyName.length != 0) {
        var autoCity = UrlBase + "AgencySearch.asmx/FetchAgencyList";
        h.txtAgencyName.autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: autoCity,
                    data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",

                    success: function (data) {
                        response($.map(data.d, function (item) {
                            var result = item.Agency_Name + "(" + item.User_Id + ")";
                            var hidresult = item.User_Id;
                            return { label: result, value: result, id: hidresult }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        alert(textStatus);
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (event, ui) {
                h.hidtxtAgencyName.val(ui.item.id);
            }
        });
    }
    var FromDateID = h.FormID.val();
    var ToDateID = h.ToID.val();
    var TxtTravelFromID = h.FormID.val();
    var TxtTravelToID = h.ToID.val();
    var dtPickerOptions = {
        numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-2y", showOtherMonths: true, selectOtherMonths: false
    };
    if (h.FormID.length != 0) {
        h.FormID.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", FromDateID.substr(0, 10));
    }
    if (h.ToID.length != 0) {
        h.ToID.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", ToDateID.substr(0, 10));
    }



    if (h.TxtTravelFrom.length != 0) {
        h.TxtTravelFrom.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", TxtTravelFromID.substr(0, 10));
    }
    if (h.TxtTravelTo.length != 0) {
        h.TxtTravelTo.datepicker(dtPickerOptions).datepicker("option", { onSelect: h.UpdateRoundTripMininumDate }).datepicker("setDate", TxtTravelToID.substr(0, 10));
    }

    h.txtAgencyName.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: autoCity,
                data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                dataType: "json", type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        var result = item.Agency_Name + "(" + item.User_Id + ")";
                        var hidresult = item.User_Id;
                        return { label: result, value: result, id: hidresult }
                    }))
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            })
        },
        autoFocus: true,
        minLength: 3,
        select: function (event, ui) {
            h.hidtxtAgencyName.val(ui.item.id);
        }
    });

    var autoRailDest = UrlBase + "CitySearch.asmx/FetchRailDestList";
    h.TxtFrom.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: autoRailDest,
                data: "{ 'RaiiCode': '" + request.term + "', maxResults: 10 }",
                dataType: "json", type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        var result = item.ALCode;
                        var hidresult = item.ALCode;
                        return { label: result, value: result, id: hidresult }
                    }))
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            })
        },
        autoFocus: true,
        minLength: 2,
        select: function (event, ui) {
           // h.hidtxtAgencyName.val(ui.item.id);
        }
    });
    h.TxtTO.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: autoRailDest,
                data: "{ 'RaiiCode': '" + request.term + "', maxResults: 10 }",
                dataType: "json", type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        var result = item.ALCode;
                        var hidresult = item.ALCode;
                        return { label: result, value: result, id: hidresult }
                    }))
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            })
        },
        autoFocus: true,
        minLength: 2,
        select: function (event, ui) {
           // h.hidtxtAgencyName.val(ui.item.id);
        }
    });
}
RailHelper.prototype.UpdateRoundTripMininumDate = function (dateText, inst) {
    SHandler.txtRetDate.datepicker("option", { minDate: dateText });
}