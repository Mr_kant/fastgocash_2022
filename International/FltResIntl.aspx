﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FltResIntl.aspx.vb" Inherits="FltResIntl"
    MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/FltSearchmdf.ascx" TagPrefix="uc1" TagName="FltSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../Styles/flightsearch.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" type="text/css" />
    <link href="../Custom_Design/css/my_design.css" rel="stylesheet" />
    <link href="../Styles/fltinl.css" rel="stylesheet" />
    <div class="w100" id="toptop">
        <div id="MainSFR">
            <div id="MainSF">
                <div class="w100">
                    <div class="" id="lftdv1">
                        <div id="fltrDiv">
                            <div id="searchtext" class="clear passenger modify-search">
                                <div class="container">
                                    <uc1:FltSearch runat="server" ID="FltSearch" />
                                </div>

                                <div class="lft bld colormn hide">
                                    <div id="divSearchText1">
                                    </div>
                                </div>
                            </div>
                            <%--<div class="modify-search">
                                <a href="#" id="hide-mod" style="float: right;">X</a>
                                <div class="container" style="margin-top: 73px;">
                                    <uc1:FltSearch runat="server" ID="FltSearch" />
                                </div>
                            </div>--%>
                            <div id="FilterLoad">
                            </div>
                            <div id="FilterBox lft">
                                <div class="jplist-panel">
                                    <div id="flterTab" style="display: none">
                                        <div id="flterTabO" class="spn1">
                                            Outbound
                                        </div>
                                        <div class="lft w2">&nbsp;</div>
                                        <div id="flterTabR" class="spn">
                                            Inbound
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="mysidenavssss" class="sidenavssss col-md-2">
                            <a href="javascript:void(0)" class="closebtnsss" onclick="closeNav()">X </a>
                            <div class="wht" id="passengersss">
                                <div id="dsplm" class="large-12 medium-12 small-12 columns jplist-panel" style="height: 59px; background: #005999; padding-top: 13px;">
                                    <a href="#" class="" style="font-size: x-large; margin-top: -9px; position: absolute; color: #fff;">Filters</a>
                                    <a href="#" class="" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="float: right; margin-right: 9px; color: #fff;">Reset All </a>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="passenger jplist-panel">
                                    <div class="clear1"></div>
                                    <div class="large-12 medium-12 small-12" id="FltrPrice">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBP" style="color: #005999 !important;">
                                            <i>
                                                <img src="../Images/hd_icons/price-tag-icon.png" style="width: 22px;" /></i>  Price
                                        </div>
                                        <div id="FBP1" class="w98 lft">
                                            <div class="clear2"></div>
                                            <div class="fo">
                                                <div class="clsone">
                                                    <div class="jplist-range-slider" data-control-type="range-slider" data-control-name="range-slider"
                                                        data-control-action="filter" data-path=".price" style="margin-left: 10px;">
                                                        <div class="clear1"></div>
                                                        <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                        </div>
                                                        <div class="clear1"></div>
                                                        <div class="lft w45">
                                                            <span class="lft">
                                                                <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                            <span class="value lft" data-type="prev-value"></span>
                                                        </div>
                                                        <div class="rgt w45">
                                                            <span class="value rgt" data-type="next-value"></span>
                                                            <span class="rgt">
                                                                <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                        </div>
                                                    </div>
                                                    <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                        data-path=".price" data-order="asc" data-type="number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fr">
                                                <div class="jplist-range-slider" data-control-type="range-sliderR" data-control-name="range-slider"
                                                    data-control-action="filter" data-path=".price">
                                                    <div class="clear1"></div>
                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                    </div>
                                                    <div class="clear1"></div>
                                                    <div class="lft w45">
                                                        <span class="lft">
                                                            <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                        <span class="value lft" data-type="prev-value"></span>
                                                    </div>
                                                    <div class="rgt w45">
                                                        <span class="value rgt" data-type="next-value"></span>
                                                        <span class="rgt">
                                                            <img src="../images/rs.png" class="rgt" style="height: 13px;" />&nbsp;</span>
                                                    </div>
                                                </div>
                                                <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                    data-path=".price" data-order="desc" data-type="number">
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="large-12 medium-12 small-12">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBS" style="color: #005999 !important;">
                                            <i>
                                                <img src="../Images/hd_icons/xxx036-2-512.png" style="width: 22px;" /></i>  Stops
                                        </div>
                                        <div class="w98 lft" id="FBS1">
                                            <div id="stopFlter" class="fo"></div>
                                            <div id="stopFlterR" class="fr"></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>


                                    <div class="clear1">
                                        <hr />
                                    </div>

                                    <div class="large-12 medium-12 small-12" id="fltrTime">
                                        <div class="clear"></div>
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBDT" style="color: #005999 !important;">
                                            <i>
                                                <img src="../Images/hd_icons/Airplane_Time-512.png" style="width: 22px;" /></i>  Departure Time
                                        </div>
                                        <div id="FBDT1" class="w98 lft " style="display: none;">
                                            <div class="fo">
                                                <div class="jplist-group"
                                                    data-control-type="DTimefilterO"
                                                    data-control-action="filter"
                                                    data-control-name="DTimefilterO"
                                                    data-path=".dtime" data-logic="or">
                                                    <div class="clear"></div>

                                                    <div class="lft w20 abc1 bdrs">
                                                        <input value="0_6" id="CheckboxT1" type="checkbox" title="Early Morning" />
                                                        <label for="CheckboxT1"></label>
                                                        <span>00 - 06</span>
                                                    </div>
                                                    <div class="lft w20 abc2t bdrs">
                                                        <input value="6_12" id="CheckboxT2" type="checkbox" title="Morning" />
                                                        <label for="CheckboxT2"></label>
                                                        <span>06 - 12</span>
                                                    </div>

                                                    <div class="lft w20 abc3t bdrs">
                                                        <input value="12_18" id="CheckboxT3" type="checkbox" title="Mid Day" />
                                                        <label for="CheckboxT3"></label>
                                                        <span>12 - 18</span>
                                                    </div>
                                                    <div class="lft w20 abc4t bdrs">
                                                        <input value="18_0" id="CheckboxT4" type="checkbox" title="Evening" />
                                                        <label for="CheckboxT4"></label>
                                                        <span>18 - 00</span>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="fr">
                                                <div class="jplist-group"
                                                    data-control-type="DTimefilterR"
                                                    data-control-action="filter"
                                                    data-control-name="DTimefilterR"
                                                    data-path=".atime" data-logic="or">

                                                    <div class="lft w20 abc1 bdrs">
                                                        <input value="0_6" id="CheckboxT1R" type="checkbox" title="Early Morning" />
                                                        <label for="CheckboxT1"></label>
                                                        <span>00 - 06</span>
                                                    </div>
                                                    <div class="lft w20 abc2t bdrs">
                                                        <input value="6_12" id="CheckboxT2R" type="checkbox" title="Morning" />
                                                        <label for="CheckboxT2"></label>
                                                        <span>06 - 12</span>
                                                    </div>

                                                    <div class="lft w20 abc3t bdrs">
                                                        <input value="12_18" id="CheckboxT3R" type="checkbox" title="Mid Day" />
                                                        <label for="CheckboxT3"></label>
                                                        <span>12 - 18</span>
                                                    </div>
                                                    <div class="lft w20 abc4t bdrs">
                                                        <input value="18_0" id="CheckboxT4R" type="checkbox" title="Evening" />
                                                        <label for="CheckboxT4"></label>
                                                        <span>18 - 00</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="large-12 medium-12 small-12">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBA" style="color: #005999 !important;">
                                            <i>
                                                <img src="../Images/hd_icons/plane-hiysd.png" style="width: 22px;" /></i>  Airline
                                        </div>
                                        <div class="w98 lft" id="FBA1">
                                            <div class="clear2"></div>
                                            <div id="airlineFilter" class="fo" style="overflow-y: scroll; height: 121px;"></div>
                                            <div id="airlineFilterR" class="fr"></div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>


                                   
                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="large-12 medium-12 small-12">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBFT" style="color: #005999 !important;">
                                            <i>
                                                <img src="../Images/hd_icons/unnamed.png" style="width: 22px;" /></i>  Fare Type
                                        </div>
                                        <div class="clsone" id="FBFT1">
                                            <div class="w98 lft">
                                                <div class="fo">
                                                    <div class="jplist-group"
                                                        data-control-type="RfndfilterO"
                                                        data-control-action="filter"
                                                        data-control-name="RfndfilteO"
                                                        data-path=".rfnd" data-logic="or">
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="r" id="CheckboxR1" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top: 3px">
                                                            <label for="CheckboxR1">Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="lft w15">
                                                            <input value="n" id="CheckboxR2" type="checkbox" />
                                                        </div>
                                                        <div class="lft w80" style="padding-top: 3px">
                                                            <label for="CheckboxR2">Non Refundable</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="clear1">
                                        <hr />
                                    </div>
                                    <div class="large-12 medium-12 small-12 columns">
                                        <div class="bld closeopen" onclick="fltrclick(this.id)" id="DAFT" style="color: #005999 !important;">
                                            <i>
                                                <img src="../Images/hd_icons/unnamed.png" style="width: 22px;" /></i>  Airline Fare Type
                                        </div>
                                        <div class="w100 lft " id="DAFT1">
                                            <div class="clear"></div>

                                            <div id="AirlineFareType" class="FareTypeO"></div>
                                            <div class="clear1">
                                                <hr />
                                            </div>

                                            <div id="AirlineFareTypeR" class="FareTypeR"></div>

                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fltbox2 col-md-10">
                            <div id="DivLoadP" class="w100" style="display: none;"></div>

                            <%--<div id="searchtext" class="clear1 passenger ">
                                <div class="lft bld colormn hide">
                                    <div id="divSearchText1">
                                    </div>
                                </div>
                            </div>--%>
                            <div class="jplist-panel">
                                <div id="divMatrixRtfO" class="divMatrix"></div>
                                <div id="divMatrixRtfR" class="hide divMatrix"></div>
                                <div id="divMatrix"></div>
                                <div class="w100 auto passenger" id="top-fix">
                                    <div class="w100 ptop10" style="background: #005999; height: 54px; width: 100%; padding: 1px 18px 0px 0px; border-radius: 4px; overflow: hidden; min-height: 60px; border: 1px solid #ccc;">
                                        <div class="lft" style="margin-top: 16px;">
                                            <div id="displaySearchinput" class="lft">
                                            </div>
                                            <div class="lft" style="color: #fff; font-weight: 600; font-size: 14px; position: relative; left: 5px; top: 2px;">
                                                <span>|</span> <span id="spanShow" onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">+ Show Net</span>
                                                <span id="spanHide" onclick="ShowHideDiscount('hide');" style="display: none;" class="spnBtnHide">- Hide Net</span>
                                            </div>

                                            <div class="rgt passenger" id="prexnt" style="margin-top: 20px; color: #000;">
                                                <div class="rgt" style="position: relative; left: 606px; top: -16px;">
                                                    <div class="auto lft">
                                                        <a href="#" id="PrevDay" class="pnday" style="color: #fff;">
                                                            <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day</a>
                                                    </div>
                                                    <div class="dotbdr lft">&nbsp;</div>
                                                    <div class="auto rgt"><a href="#" id="NextDay" class="pnday" style="color: #fff;">Next Day&nbsp;<img src="../Styles/images/closeopen.png" /></a></div>
                                                </div>
                                            </div>
                                            <div class="mod-ser" style="position: absolute; top: 21px; right: 10%;">
                                                <span id="show-mod"><i class="fa fa-edit fa-2x" style="color: #fff;"></i></span>&nbsp;&nbsp;
                                                 <span id="show-filter"><i class="fa fa-filter fa-2x" aria-hidden="true" style="color: #fff;"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="RoundTripH" class="flightbox">
                                <div class="nav-container">
                                    <div class="jplist-panel">
                                        <div class="clear"></div>
                                        <div id="fltselct" class="hide">
                                            <div class="f16">Your Selection</div>
                                            <div id="selctcntnt" class="mauto">
                                                <div id="fltgo" class="w40 lft brdrtop">
                                                </div>
                                                <div class="w5 lft">&nbsp;</div>
                                                <div id="fltbk" class="w40 lft brdrtop">
                                                </div>
                                                <div id="fltbtn" class="w15 rgt">
                                                    <span class="f16 bld rgt" id="totalPay"></span>
                                                    <div class="clear"></div>
                                                    <input type="button" value="Book" class="buttonfltbk" id="FinalBook" />
                                                    <div id="Divproc" class="hide bld">
                                                        <img alt="Booking In Progress" width="50" height="50" src="../Images/loading_bar.gif" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="w100 mauto bld bgmn1 colorwht">
                                            <div class="w50 padding1 lft">
                                                <div id="RTFTextFrom" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfFromPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfFromNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="w45 padding1 lft">
                                                <div id="RTFTextTo" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfToPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfToNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="headerow hide">
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ1"
                                                    data-control-name="sortCITZ1"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirlineR"
                                                    data-control-name="sortAirlineR"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptimeR"
                                                    data-control-name="sortDeptimeR"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtimeR"
                                                    data-control-name="sortArrtimeR"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdurR"
                                                    data-control-name="sortTotdurR"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZR"
                                                    data-control-name="sortCITZR"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="lft w51">
                                    <div id="divFrom1" class="listO w100">
                                    </div>
                                </div>
                                <div class="rgt w50">
                                    <div id="divTo1" class="listR w100">
                                    </div>
                                </div>
                            </div>
                            <div class="flightbox" style="display: block;" id="onewayH">
                                <div class="nav-container">
                                    <div class="jplist-panel">
                                        <div class="headerow hide">
                                            <div class="w15 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Departure
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w24 lft">
                                                &nbsp;
                                            </div>
                                            <div class="w15 colorwht rgt srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ"
                                                    data-control-name="sortCITZ"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clear">
                                </div>
                                <div class="nav-container">
                                    <div class="nav">

                                        <div class="jplist-panel">

                                            <div class="headerow">
                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">
                                                    <div class=""
                                                        data-control-type="sortAirline"
                                                        data-control-name="sortAirline"
                                                        data-control-action="sort"
                                                        data-path=".airlineImage"
                                                        data-order="asc"
                                                        data-type="text">
                                                        Airline <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>
                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortDeptime"
                                                        data-control-name="sortDeptime"
                                                        data-control-action="sort"
                                                        data-path=".deptime"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -37px;">
                                                        Departure <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>
                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortArrtime"
                                                        data-control-name="sortArrtime"
                                                        data-control-action="sort"
                                                        data-path=".arrtime"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -43px;">
                                                        Arrival <i class="fa fa-caret-up"></i>
                                                    </div>

                                                </div>
                                                <div class="large-4 medium-4 small-4 columns passenger abdd colorwht  srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortTotdur"
                                                        data-control-name="sortTotdur"
                                                        data-control-action="sort"
                                                        data-path=".totdur"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -85px;">
                                                        Duration <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>

                                                <div class="large-2 medium-2 small-3 columns abdd colorwht srtarw" onclick="myfunction(this)">
                                                    <div
                                                        class=""
                                                        data-control-type="sortCITZ"
                                                        data-control-name="sortCITZ"
                                                        data-control-action="sort"
                                                        data-path=".price"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -246px;">
                                                        Fare <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="mainDiv">
                                    <div id="divResult">
                                        <div id="divFrom" class="list" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                        </div>
                        <div class="clear">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="render">
        </div>
        <div class="clear">
        </div>
    </div>
    <div id="waitMessage" style="display: none;">
        <div style="text-align: center; z-index: 1001; font-size: 12px; font-weight: bold; padding: 20px; border-radius: 0px;">
            <div id="loader" style="display: none;">
                <div id="plane">
                    <i>
                        <img src="../Images/icons/146552.png" style="width: 60px;" /></i>
                </div>
            </div>
            <div class="animation">
                <svg id="master-artboard" class="animation__cloud--back" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px">
                    <path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)" />
                </svg>
                <svg id="Svg1" class="animation__cloud--middle" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px">
                    <path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)" />
                </svg>
                <div class="animation__plane--shadow"></div>

                <svg class="animation__plane" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" data-name="svgi-plane" viewBox="0 0 135.67 49.55">
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" d="M74.663 45.572h-9.106z" class="svgi-plane--stripe3" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M.75 26.719h23.309z" class="svgi-plane--stripe1" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M11.23 31.82h22.654z" class="svgi-plane--stripe2" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 53.47597,24.263013 h 68.97869 c 6.17785,0 12.47074,6.758518 12.40872,8.67006 -0.05,1.537903 -5.43763,7.036166 -11.72452,7.056809 l -59.599872,0.201269 c -9.092727,0.03097 -23.597077,-5.992662 -22.178652,-11.794378 1.160348,-4.74789 7.862358,-4.13376 12.115634,-4.13376 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M 45.243501,24.351777 37.946312,0.952937 h 7.185155 c 15.37061,20.184725 28.402518,23.28324 28.402518,23.28324 0,0 -27.106129,-0.178562 -28.290484,0.1156 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 42.699738,18.984597 h 10.627187 c 5.753726,0 5.364609,7.799958 0,7.799958 H 42.998828 c -4.96749,0 -5.574672,-7.799958 -0.29909,-7.799958 z m 33.139939,16.164502 h 29.656893 c 6.62199,0 6.49395,6.577892 0,6.577892 H 75.940707 c -8.658596,0 -8.499549,-6.35598 -0.10103,-6.577892 z m 9.693907,6.664592 h 8.86866 c 4.439332,0 4.309293,7.066099 0,7.066099 h -8.756626 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 85.55159,42.447431 c 0,0 -5.282585,0.456211 -6.372912,3.263659 1.335401,2.378073 6.397919,2.528767 6.397919,2.528767 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 133.68903,31.07417 h -7.01411 c -1.26938,0 -2.89286,-1.005314 -3.21496,-3.216179 h 7.50225 z" />
                    <ellipse cx="113.564" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="107.56219" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="101.56039" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="95.558594" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="89.556793" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="83.554993" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="77.553192" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="71.551392" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="65.549591" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                </svg>
                <svg id="Svg2" class="animation__cloud--front" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px">
                    <path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)" />
                </svg>
                <div class="animation__loader"></div>
            </div>
            <footer>

    <div id="searchquery" style="color: #fff;  font-size: 18px;z-index:10111111;" >
                </div>
</footer>
        </div>
    </div>
    <div id="backgroundPopup">
    </div>
    <div id="toPopup" class="flight_head">
        <div class="close">
        </div>
        <span class="ecs_
            ">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: center; color: #FFFFFF; background-color: #20313f; font-weight: bold; padding-top: 5px; padding-bottom: 5px;">Send Mail</h4>
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;"></td>
                    <td class="textsmall">
                        <input type='radio' name='choices' checked="checked" value='A' />
                        All Result
                         <input type='radio' name='choices' value='S' />Selected Result
                    </td>
                </tr>

                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">From:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtFromMail" name="txtFromMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">To:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtToMail" name="txtToMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Subject:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtSubj" name="txtSubj" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Message:
                    </td>
                    <td>
                        <textarea id="txtMsg" class="headmail" name="txtMsg" rows="4" cols="20"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 20px;"></td>
                    <td align="right">
                        <input type="button" class="buttonfltbk" id="btnSendMail" name="btnSendMail" value="Send Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label id="lblMailStatus" style="display: none; color: Red;">
                        </label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="FareBreakupHeder" class="modal" tabindex="-1" role="dialog" aria-labelledby="FareBreakupHederLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FareBreakupHederLabel">Fare Summary</h5>
                    <button type="button" class="close FareBreakupHederClose" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" id="FareBreakupHederId" style="padding: 2px 10px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary FareBreakupHederClose">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="ConfmingFlight" class="CfltFare" style="display: none;">
        <div id="divLoadcf" class="">
        </div>
    </div>
    <div class="clear"></div>
    <a href="#toptop"><span class="toptop" style="position: fixed; bottom: 4px; right: 20px; height: 50px; font-size: 20px; width: 50px; border-radius: 50%; cursor: pointer; padding: 13px 15px; background: rgb(0, 75, 145); color: rgb(255, 255, 255); display: block;"><i class="fa fa-chevron-up" aria-hidden="true"></i></span></a>
    <div class="clear1"></div>
    <input type="hidden" id="hdnMailString" name="hdnMailString" />
    <input type="hidden" id="hdnAllOrSelecte" name="hdnAllOrSelecte" />
    <input type="hidden" id="hdnOnewayOrRound" name="hdnOnewayOrRound" />
    <asp:Literal ID="henAgcDetails" runat="server" Visible="false"></asp:Literal>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#hide-mod").click(function () {
                $(".modify-search").hide();
            });
            $("#show-mod").click(function () {
                $(".modify-search").show();
            });

            $("#show-mod1").click(function () {
                $(".modify-search").show();
            });

            $("#show-filter").click(function () {
                $(".sidenavssss").show();
            });

            $("#show-filter1").click(function () {
                $(".sidenavssss").show();
            });


            $("#closebtnsss1").click(function () {
                $(".sidenavssss").hide();
            });
        });
    </script>

    <script type="text/javascript">
        function openNav() {

            document.getElementById("mysidenavssss").style.width = "375px";
            document.getElementById("passengersss").style.display = "block";

        }

        function closeNav() {
            document.getElementById("mysidenavssss").style.width = "0";
            document.getElementById("passengersss").style.display = "none";
        }
    </script>

    <script type="text/javascript">
        //jQuery(function ($) {
        //    $(window).scroll(function fix_element() {
        //        $('#passengersss').css(
        //            $(window).scrollTop() > 100
        //                ? { 'position': 'fixed', 'top': '10px', 'width': '210px', 'overflow-y': 'scroll', 'height': '570px', 'overflow-x': 'hidden' }
        //                : { 'position': 'relative', 'top': 'auto', 'overflow-y': 'hidden', 'height': 'auto', 'overflow-x': 'hidden' }
        //        );
        //        return fix_element;
        //    }());
        //});




        //jQuery(function ($) {
        //    $(window).scroll(function fix_element() {
        //        $('#top-fix').css(
        //            $(window).scrollTop() > 100
        //                ? { 'position': 'fixed', 'top': '6px', 'z-index': '999', 'width': '81%' }
        //                : { 'position': 'relative', 'top': 'auto', 'width': '100%' }
        //        );
        //        return fix_element;
        //    }());
        //});







    </script>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/")%>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/JSLINQ.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jplist.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/SortAD.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/TextFilterGroup.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/handleQueryString.js?v=4.9")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/async.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/LZCompression.js?y=1.8")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew.js?v=1.2")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.tooltip.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightMailing.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/pako.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".abc1").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT1") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc2t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT2") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc3t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT3") {
                    $(this).toggleClass("bdrss");
                }

            });
            $(".abc4t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT4") {
                    $(this).toggleClass("bdrss");
                }

            });

        });
    </script>
    <script type="text/javascript">
        jQuery("document").ready(function ($) {
            debugger;
            var nav = $('.nav-container');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 175) {
                    $(".toptop").fadeIn();
                    if ($("#lftdv1").is(":visible") == false) {
                        nav.addClass("f-nav1");
                    }
                    else {
                        nav.addClass("f-nav");
                    }
                } else {
                    $(".toptop").fadeOut();
                    nav.removeClass("f-nav");
                    nav.removeClass("f-nav1");
                }
            });



        });
    </script>
    <script type="text/javascript">

        jQuery("document").ready(function ($) {
            debugger;

            $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");

            $("#rdbOneWay").click(function () {

                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
                $(".sk").removeAttr(disabled = "disabled");
            });

            $("#rdbRoundTrip").click(function () {

                $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");

            });
            $("#rdbMultiCity").click(function () {
                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });

            var nav = $('.nav-container');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 175) {
                    $(".toptop").fadeIn();
                    if ($("#lftdv1").is(":visible") == false) {
                        nav.addClass("f-nav1");
                    }
                    else {
                        nav.addClass("f-nav");
                    }
                } else {
                    $(".toptop").fadeOut();
                    nav.removeClass("f-nav");
                    nav.removeClass("f-nav1");
                }
            });

            var Triptype = getUrlVars()["TripType"];
            if (Triptype == "rdbOneWay") {
                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            }



            //if (Triptype == "rdbOneWay") {
            //    $(".onewayss").removeAttr(disable = "disable");
            //}

            //if (Triptype == "rdbRoundTrip") {
            //    $(".onewayss").removeAttr(disable = "disable");
            //}


            if (Triptype == "rdbRoundTrip") {
                $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
            }
        });
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
    </script>

</asp:Content>
