﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="privacy.aspx.vb" Inherits="privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container">

<h2>Privacy policy</h2>

    <p>Your privacy is important to us. To better protect your privacy Policy we provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice easy to find, we make it available on our homepage and at every point where personally identifiable information may be requested.</p>

    <p style="font-weight:600;">The information we collect:</p>

    <p>This notice applies to all information collected or submitted through www.fastgocash.com. The website offers travel related services such as air tickets, Domestic Money Transfer etc. We collect the following personal information to provide the services mentioned above.</p>
    <p>Name</p>
    <p>Address</p>
    <p>Date of Birth</p>
    <p>Passport Number</p>
    <p>Email Address</p>
    <p>Phone Number</p>
    <p>IP Address</p>
    

    <p style="font-weight:600;">The way we use information</p>

    <p>we use the information you provide about yourself when placing an order only to complete that order. We do not share this information with outside parties except to the extent necessary to complete that order. We will use this information to occasionally inform you of promotions being run on the website. Within each email, you will be provided with an option to opt out from receiving such emails in the future. We use return email addresses to answer the email we receive. Such addresses are not used for any other purpose and are not shared with outside parties. We use non-identifying and aggregate information to better design our website and to share with advertisers. For example, we may tell an advertiser that x number of individuals visited a certain area on our website, but we would not disclose anything that could be used to identify those individuals. Finally, we never use or share the personally identifiable information provided to us online in ways unrelated to the ones described above without also providing you with an opportunity to opt-out or otherwise prohibit such unrelated uses.</p>


    <p style="font-weight:600;">Our commitment to data security</p>
    <p>To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect online.</p>

    <p style="font-weight:600">How you can access or correct your information</p>
    <p>You can access all your personally identifiable information that we collect online and maintain by contacting our customer service department. We use this procedure to better safeguard your information. You can correct factual errors in your personally identifiable information by sending us a request that credibly shows error. To protect your privacy and security, we will also take reasonable steps to verify your identity before granting access or making corrections.</p>

    <p style="font-weight:600">How to contact us</p>
    <p>Should you have other questions or concerns about these privacy policies, please call us at +917409455555 or send us an email at info@fastgocash.com</p>

    </div>

</asp:Content>

