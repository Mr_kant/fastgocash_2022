﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Report_Dmt1_recharge_trans_report :  System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindDMTTranctionReport();
        }
    }

    protected void BindDMTTranctionReport()
    {
        DataTable dtRecord = GetRechargeTrancReportList(null, null, null, null, null);
        BindListView(dtRecord);
    }

    private void BindListView(DataTable dtRecord)
    {
        if (dtRecord != null && dtRecord.Rows.Count > 0)
        {
            Session["TransRecord"] = dtRecord;
            PagingButtom.Visible = true;
        }
        else
        {
            Session["TransRecord"] = null;
            PagingButtom.Visible = false;
        }

        lstTransaction.DataSource = dtRecord;
        lstTransaction.DataBind();
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        string transtype = ddltransType.SelectedValue;
        BindListView(GetRechargeTrancReportList(Request["From"].ToString(), Request["To"].ToString(), txt_Trackid.Text, transtype, status));
    }
    protected void lstTransaction_PagePropertiesChanged(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        BindListView(Session["TransRecord"] as DataTable);
    }
    protected void btn_export_Click(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        string transtype = ddltransType.SelectedValue;

        DataTable dtRecord = new DataTable();
        if (Session["TransRecord"] != null && string.IsNullOrEmpty(Request["From"].ToString()) && string.IsNullOrEmpty(Request["To"].ToString()))
        {
            dtRecord = Session["TransRecord"] as DataTable;
        }
        else
        {
            dtRecord = GetRechargeTrancReportList(Request["From"].ToString(), Request["To"].ToString(), txt_Trackid.Text, transtype, status);
        }
        dtRecord = GetDataorExport(dtRecord);
        DataSet dsExport = new DataSet();
        dsExport.Tables.Add(dtRecord);
        STDom.ExportData(dsExport, ("Recharge_Bil_Trans_Report_" + DateTime.Now));
    }

    private DataTable GetRechargeTrancReportList(string fromDate = null, string toDate = null, string trackid = null, string transtype = null, string status = null)
    {
        DataTable dtDueReport = new DataTable();

        try
        {
            string newfromdate = string.Empty; string newtodate = string.Empty;

            if (!string.IsNullOrEmpty(fromDate))
            {
                newfromdate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                newtodate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }

            adap = new SqlDataAdapter("sp_GetRchargeTransDetails", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", Session["UID"].ToString());
            adap.SelectCommand.Parameters.AddWithValue("@ClientRefId", !string.IsNullOrEmpty(trackid) ? trackid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@TransType", !string.IsNullOrEmpty(transtype) ? transtype.ToUpper().Trim() : null);           
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(newfromdate) ? newfromdate.Trim() : "");
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(newtodate) ? newtodate.Trim() + " 23:59:59" : null);
            adap.SelectCommand.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(status) ? status.Trim() : null);
            adap.Fill(dtDueReport);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dtDueReport;
    }

    private DataTable GetDataorExport(DataTable dtTrans)
    {
        DataTable mytable = new DataTable();

        try
        {
            mytable.Columns.Add("AgentId", typeof(string));
            mytable.Columns.Add("ClientRef Id", typeof(string));
            mytable.Columns.Add("Number", typeof(string));
            mytable.Columns.Add("ServiceType", typeof(string));
            mytable.Columns.Add("Operator", typeof(string));
            mytable.Columns.Add("Amount", typeof(string));
            mytable.Columns.Add("OrderID", typeof(string));
            mytable.Columns.Add("Status", typeof(string));          
            mytable.Columns.Add("Trans Date", typeof(string));

            if (dtTrans != null && dtTrans.Rows.Count > 0)
            {
                for (int i = 0; i < dtTrans.Rows.Count; i++)
                {
                    DataRow dr1 = mytable.NewRow();
                    dr1 = mytable.NewRow();
                    dr1["AgentId"] = dtTrans.Rows[i]["Agentid"].ToString();
                    dr1["ClientRef Id"] = dtTrans.Rows[i]["ClientRefId"].ToString();
                    dr1["Number"] = dtTrans.Rows[i]["Number"].ToString();
                    dr1["ServiceType"] = dtTrans.Rows[i]["ServiceType"].ToString();
                    dr1["Operator"] = dtTrans.Rows[i]["Operator"].ToString();
                    dr1["Amount"] = dtTrans.Rows[i]["Amount"].ToString();
                    dr1["OrderID"] = dtTrans.Rows[i]["TransactionId"].ToString();
                    dr1["Status"] = dtTrans.Rows[i]["Status"].ToString();                 
                    dr1["Trans Date"] = dtTrans.Rows[i]["UpdatedDate"].ToString();
                    mytable.Rows.Add(dr1);
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return mytable;
    }
}