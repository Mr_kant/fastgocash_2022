﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="UploadAmount.aspx.cs" Inherits="SprReports_Accounts_UploadAmount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

   
 
    <%--<link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>"
        rel="stylesheet" />--%>
    <%--<style>
        .msi {
            width: 130% !important;
            max-width: 130% !important;
        }
        #ctl00_ContentPlaceHolder1_rblPaymentMode {
            margin:0 auto;
        }
    </style>--%>

    




    <div class="container">

      <div class="card-header">
            <div class="col-md-9">
                <h3 style="text-align:center;color:orange;">Wallet Top-Up By Payment Gateway</h3>
                <hr style="height:3px;background:orange;"/>
            </div>
          </div>
      
        <div class="row ">
            <div class="col-md-9">
              
                    <div class="row">
                    <div class="col-md-4">
                        <label>Enter Amount</label>
                        <asp:TextBox ID="TxtAmout" runat="server" class="form-c ontrolaa" onkeypress="return keyRestrict(event,'0123456789');" MaxLength="11" Text="0" AutoCompleteType="Disabled" style="color:#000;"></asp:TextBox>
                    </div>
                        </div>

                    <div class="row">
                        <div class="col-md-4">
                        Transaction Charges: &nbsp; &nbsp;<asp:Label ID="lblTransCharges" runat="server"></asp:Label>
                             </div>   
                        </div>
                    <div class="row">
                        <div class="col-md-4">
                        Total  Amount:&nbsp; &nbsp;<asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                        </div>
                        </div>



                    <div class="row">
                    <div class=" ">
                       
                         <div class="card-header">
            <div class="col-md-12">
                <h2 style="text-align: center;">Payment Mode</h2>
                <hr />
            </div>
          </div>
                        <hr />

                        

                        <asp:RadioButtonList ID="rblPaymentMode" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Credit Card" Value="creditcard" Selected="True">&nbsp;<img src="../../Images/icons/credit_card_PNG107.png" title="Credit Card" style="width: 42px;"/></asp:ListItem>
                            <asp:ListItem Text="Debit Card" Value="debitcard">&nbsp;<img src="../../Images/icons/Debit.png" title="Debit Card" style="width: 42px;"/></asp:ListItem>
                            <asp:ListItem Text="Net Banking" Value="netbanking">&nbsp;<img src="../../Images/icons/net-banking-png.png" title="Net Banking" style="width: 42px;"/></asp:ListItem>
                            <asp:ListItem Text="UPI" Value="upi">&nbsp;<img src="../../Images/icons/UPI.png" title="UPI" style="width: 42px;"/></asp:ListItem>
                            <asp:ListItem Text="Wallets" Value="cashcard">&nbsp;<img src="../../Images/icons/21.png" style="width: 42px;"/></asp:ListItem>
                            <asp:ListItem Text="Paytm" Value="Paytm">&nbsp;<img src="../../Images/icons/paytm-512.png"  style="width: 42px;"/></asp:ListItem>
                            <asp:ListItem Text="FreeCharge" Value="Freecharge">&nbsp;<img src="../../Images/icons/yfuyfguyu.png" style="width: 42px;"/></asp:ListItem>
                            <%--<asp:ListItem Text="MobiKwik" Value="Mobikwik"></asp:ListItem>--%>
                        </asp:RadioButtonList>
                    </div>
                        </div>
                    <br />

                    <div class="col-md-2">
                            <asp:Button ID="BtnUpload" runat="server" CssClass="btn btn-danger" Text="Upload Amount" OnClick="BtnUpload_Click"/>
                        </div>

                    
                    <%-- <div class="col-md-4  text-center ">
                </div>--%>
             


            </div>
        </div>
        <br />


        <input type="hidden" id="TransCharges" name="TransCharges" />
        <input type="hidden" id="TotalAmount" name="TotalAmount" />
    </div>

    <br />
    

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

    <%-- <script type="text/javascript">        
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_TxtAmout").keypress(function () {
                GetPgTransCharge();
            });
        });
</script>--%>

    <script type="text/javascript">
        //function preventBack() { window.history.forward(); }
        //setTimeout("preventBack()", 0);
        //window.onunload = function () { null };
        //window.onload = function () {
        //    document.getElementById("ctl00_ContentPlaceHolder1_TxtAmout").name = "txt" + Math.random();
        //}
    </script>

    <script type="text/javascript">
        function ValidateAmount() {
            if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() == "") {
                alert("Plese enter upload amount.")
                return false;
            }
            if (parseFloat($("#ctl00_ContentPlaceHolder1_TxtAmout").val()) < 1) {
                alert("Plese enter amount greater than zero.")
                return false;
            }
            GetPgTransCharge();

            var bConfirm = confirm('Payment gateway transaction charges Rs. ' + $('#TransCharges').val() + ' and  Total Amount Rs. ' + $('#TotalAmount').val() + ' debit from your bank a/c, Are you sure upload amount?');
            if (bConfirm == true) {
                $("#ctl00_ContentPlaceHolder1_BtnUpload").hide();
                return true;
            }
            else {
                $("#ctl00_ContentPlaceHolder1_BtnUpload").show();
                return false;
            }
        }

        $("#ctl00_ContentPlaceHolder1_TxtAmout").keyup(function () {
            GetPgTransCharge();
            //if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() == "" && $("#ctl00_ContentPlaceHolder1_TxtAmout").val() == null) {
            //    var str = 0;
            //    $("#ctl00_ContentPlaceHolder1_TxtAmout").val(str);
            //}            
        })

        $("#ctl00_ContentPlaceHolder1_rblPaymentMode").click(function () {
            GetPgTransCharge();
        });
        function GetPgTransCharge() {
            var checked_radio = $("[id*=ctl00_ContentPlaceHolder1_rblPaymentMode] input:checked");
            var PaymentMode = checked_radio.val();
            var TotalPgCharges = 0;
            var TotalAmount = 0;
            var Amount = 0;
            if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() != "" && $("#ctl00_ContentPlaceHolder1_TxtAmout").val() != null) {
                Amount = $("#ctl00_ContentPlaceHolder1_TxtAmout").val();
            }
            else {
                Amount = 0
            }
            $.ajax({
                type: "POST",
                url: "UploadAmount.aspx/GetPgChargeByMode",
                data: '{paymode: "' + PaymentMode + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d != "") {
                        if (data.d.indexOf("~") > 0) {
                            //var res = data.d.split('~');
                            var PgCharge = data.d.split('~')[0]
                            var chargeType = data.d.split('~')[1]
                            if (chargeType == "F") {
                                //calculate fixed pg charge 
                                TotalPgCharges = (parseFloat(PgCharge)).toFixed(2);
                                TotalAmount = (parseFloat(Amount) + parseFloat(TotalPgCharges)).toFixed(2);

                                $('#TransCharges').val(TotalPgCharges);
                                $('#TotalAmount').val(TotalAmount);
                                $('#<%=lblTransCharges.ClientID%>').html(TotalPgCharges);
                                $('#<%=lblTotalAmount.ClientID%>').html(TotalAmount);
                            }
                            else {
                                //calculate percentage pg charge                                     
                                TotalPgCharges = ((parseFloat(Amount) * parseFloat(PgCharge)) / 100).toFixed(2);
                                TotalAmount = (parseFloat(Amount) + parseFloat(TotalPgCharges)).toFixed(2);

                                $('#TransCharges').val(TotalPgCharges);
                                $('#TotalAmount').val(TotalAmount);
                                $('#<%=lblTransCharges.ClientID%>').html(TotalPgCharges);
                                     $('#<%=lblTotalAmount.ClientID%>').html(TotalAmount);
                            }
                        }
                    }
                    else {
                        alert("try again");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>

</asp:Content>

