﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="false" CodeFile="RailBooking.aspx.vb" Inherits="Report_RailBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
        .form-group > label{
            color:#004b91 !important;
            font-weight:bold;
        }
    </style>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>

    <div class="row" style="margin-left: -136px;">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Rail > Rail Booking Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Booking From Date</label>
                                    <asp:TextBox ID="FromID" CssClass="form-control" runat="server"  autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Booking To Date</label>
                                    <asp:TextBox ID="ToID" CssClass="form-control" runat="server" autocomplete="off" ></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PNR</label>
                                    <asp:TextBox ID="txt_PNR" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Transaction ID</label>
                                    <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Travel From Date</label>
                                    <asp:TextBox ID="TxtTravelFrom" runat="server" CssClass="form-control"  autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Travel To Date</label>
                                    <asp:TextBox ID="TxtTravelTo" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Train No</label>
                                    <asp:TextBox ID="TxtTrainNo" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Booking Class</label>
                                    <asp:TextBox ID="TxtBookingClass" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Station Code</label>
                                    <asp:TextBox ID="TxtFrom" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Station Code</label>
                                    <asp:TextBox ID="TxtTO" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 pull-right">
                                <div class="w40 lft">
                                    <asp:Button ID="BTN_RAIL" runat="server" Text="Search" CssClass="button buttonBlue" />
                                </div>
                                <div class="w20 lft">&nbsp;</div>

                                <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                            </div>
                           
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Total FGC Sale :</label>
                                <asp:Label ID="lbl_Total" runat="server">0.00</asp:Label>
                            </div>
                        </div>
                       
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Total Ticket Issued :</label>
                                    <asp:Label ID="lbl_counttkt" runat="server">0</asp:Label>
                                </div>
                            </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-9">
                            <asp:RadioButtonList ID="rbHDR" runat="server" Visible="false">

                                <asp:ListItem Text="Yes" Value="Yes" Selected="True">

                                </asp:ListItem>

                                <asp:ListItem Text="No" Value="No"></asp:ListItem>

                            </asp:RadioButtonList>



                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" CssClass="table">
                                <Columns>

                                    <asp:BoundField DataField="Transaction_ID" HeaderText="Transaction ID" />
                                    <asp:BoundField DataField="PNR_Number" HeaderText="PNR" />
                                    <asp:BoundField DataField="Train_No" HeaderText="Train No" />
                                    <asp:BoundField DataField="ORIGIN" HeaderText="From" />

                                    <asp:BoundField DataField="DESTINATION" HeaderText="To" />
                                    <asp:BoundField DataField="Travel_Date" HeaderText="Travel Date" />


                                   <%-- <asp:BoundField DataField="Irctc_Amount" HeaderText="Irctc Amount" />--%>
                                   <%-- <asp:BoundField DataField="Booked_By" HeaderText="IRCTC ID" />
                                    <asp:BoundField DataField="FGDID" HeaderText="FGC ID" />--%>
                                    <asp:BoundField DataField="Booking_Date" HeaderText="Booking Date" />

                                    <asp:BoundField DataField="Booking_Class" HeaderText="Class" />
                                    <asp:BoundField DataField="Booking_Amount" HeaderText="Net Amount" />
                                   <%-- <asp:BoundField DataField="FGCProfit" HeaderText="Profit" />
                                    <asp:BoundField DataField="GroupType" HeaderText="Group" />--%>




                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="col-md-9">
                            <span style="color: #FF0000">* N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/RailBooking.js") %>"></script>

</asp:Content>