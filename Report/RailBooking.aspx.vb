﻿Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Partial Class Report_RailBooking
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        Else
            Try
                If Not IsPostBack Then
                    ''FromID.Text = System.DateTime.Now().ToString("dd-MM-yyyy")
                    '' ToID.Text = System.DateTime.Now().ToString("dd-MM-yyyy")

                    txt_OrderId.Text = ""
                    txt_PNR.Text = ""
                    BindEmpGrid()
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)

            End Try
        End If
    End Sub

    Protected Sub BTN_RAIL_Click(sender As Object, e As EventArgs) Handles BTN_RAIL.Click
        Try
            BindEmpGrid()
            empityrext()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Private Sub empityrext()

        txt_OrderId.Text = ""
        txt_PNR.Text = ""

        TxtTrainNo.Text = ""
        TxtBookingClass.Text = ""
        TxtFrom.Text = ""
        TxtTO.Text = ""
        TxtTravelFrom.Text = ""
        TxtTravelTo.Text = ""
        FromID.Text = ""
        ToID.Text = ""

    End Sub

    Private Sub BindEmpGrid()
        lbl_Total.Text = 0
        lbl_counttkt.Text = 0

        Dim dt As New DataTable()
        Try
            Dim objSqlTrans As New SqlTransactionDom
            Dim ds As New DataSet()
            Dim strDate As String = ""
            Dim strEDate As String = ""

            Dim TravelFrom As String = ""
            Dim TravelTo As String = ""
            Dim TxtOrigin As String = ""
            Dim TxtDest As String = ""
            Dim TrainNo As String = ""
            Dim TravelClass As String = ""

            TrainNo = TxtTrainNo.Text
            TravelClass = TxtBookingClass.Text
            TxtOrigin = TxtFrom.Text
            TxtDest = TxtTO.Text

            If FromID.Text = "" Then
                ''strDate = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                strDate = Convert.ToDateTime(FromID.Text.Split("-")(2) + "-" + FromID.Text.Split("-")(1) + "-" + FromID.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 00:00:00.000"
            End If
            If ToID.Text = "" Then
                ''strEDate = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                strEDate = Convert.ToDateTime(ToID.Text.Split("-")(2) + "-" + ToID.Text.Split("-")(1) + "-" + ToID.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 23:59:59.000"
            End If

            If TxtTravelFrom.Text = "" Then
                ''TravelFrom = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                TravelFrom = Convert.ToDateTime(TxtTravelFrom.Text.Split("-")(2) + "-" + TxtTravelFrom.Text.Split("-")(1) + "-" + TxtTravelFrom.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 00:00:00.000"
            End If
            If TxtTravelTo.Text = "" Then
                ''TravelTo = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                TravelTo = Convert.ToDateTime(TxtTravelTo.Text.Split("-")(2) + "-" + TxtTravelTo.Text.Split("-")(1) + "-" + TxtTravelTo.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 23:59:59.000"
            End If


            ds = objSqlTrans.GetRailBookingReportNew(Session("AgencyId"), txt_OrderId.Text, strDate, strEDate, txt_PNR.Text,
            TravelFrom, TravelTo, TrainNo, TravelClass, TxtOrigin, TxtDest)
            If ds IsNot Nothing Then
                dt = ds.Tables(0)
            End If
            If dt.Rows.Count > 0 Then

                Dim Amount As Integer
                Dim IRCTAmount As Integer
                Dim Profit As Integer
                Amount = dt.Compute("SUM(Booking_Amount)", "")
                Profit = dt.Compute("SUM(FGCProfit)", "")
                IRCTAmount = dt.Compute("SUM(Irctc_Amount)", "")
                Dim Count As Object
                Count = dt.Rows.Count

                lbl_Total.Text = Amount
                lbl_counttkt.Text = Count

                GridView1.DataSource = dt
                GridView1.DataBind()
            Else
                GridView1.DataSource = Nothing
                GridView1.DataBind()
            End If
        Catch ex As Exception
            Response.Write("Error Occured: " & ex.ToString())
        Finally
            dt.Clear()
            dt.Dispose()
        End Try
    End Sub
    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        lbl_Total.Text = "0"
        lbl_counttkt.Text = "0"
        Dim dt As New DataTable()
        Try
            Dim objSqlTrans As New SqlTransactionDom
            Dim ds As New DataSet()
            Dim strDate As String = ""
            Dim strEDate As String = ""

            Dim TravelFrom As String = ""
            Dim TravelTo As String = ""
            Dim TxtOrigin As String = ""
            Dim TxtDest As String = ""
            Dim TrainNo As String = ""
            Dim TravelClass As String = ""

            TrainNo = TxtTrainNo.Text
            TravelClass = TxtBookingClass.Text
            TxtOrigin = TxtFrom.Text
            TxtDest = TxtTO.Text

            If FromID.Text = "" Then
                ''strDate = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                strDate = Convert.ToDateTime(FromID.Text.Split("-")(2) + "-" + FromID.Text.Split("-")(1) + "-" + FromID.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 00:00:00.000"
            End If
            If ToID.Text = "" Then
                ''strEDate = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                strEDate = Convert.ToDateTime(ToID.Text.Split("-")(2) + "-" + ToID.Text.Split("-")(1) + "-" + ToID.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 23:59:59.000"
            End If

            If TxtTravelFrom.Text = "" Then
                ''TravelFrom = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                TravelFrom = Convert.ToDateTime(TxtTravelFrom.Text.Split("-")(2) + "-" + TxtTravelFrom.Text.Split("-")(1) + "-" + TxtTravelFrom.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 00:00:00.000"
            End If
            If TxtTravelTo.Text = "" Then
                ''TravelTo = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                TravelTo = Convert.ToDateTime(TxtTravelTo.Text.Split("-")(2) + "-" + TxtTravelTo.Text.Split("-")(1) + "-" + TxtTravelTo.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 23:59:59.000"
            End If

            ds = objSqlTrans.GetRailBookingReportNew(Session("AgencyId"), txt_OrderId.Text, strDate, strEDate, txt_PNR.Text,
                                                     TravelFrom, TravelTo, TrainNo, TravelClass, TxtOrigin, TxtDest)
            If ds IsNot Nothing Then
                dt = ds.Tables(0)
                ds.Tables(0).Columns.Remove(ds.Tables(0).Columns("SNo"))
                ds.Tables(0).Columns.Remove(ds.Tables(0).Columns("Irctc_Amount"))
                ds.Tables(0).Columns.Remove(ds.Tables(0).Columns("Booked_By"))
                ds.Tables(0).Columns.Remove(ds.Tables(0).Columns("FGDID"))
                ds.Tables(0).Columns.Remove(ds.Tables(0).Columns("FGCProfit"))
                ds.Tables(0).Columns.Remove(ds.Tables(0).Columns("GroupType"))

            End If

            STDom.ExportData(ds)
        Catch ex As Exception
            ''Response.Write("Error Occured: " & ex.ToString())
        Finally
            dt.Clear()
            dt.Dispose()
        End Try

    End Sub

End Class
