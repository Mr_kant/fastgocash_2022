﻿
Imports System.Data

Partial Class Report_RailBookingReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        Else
            Try
                If Not IsPostBack Then

                    BindEmpGrid()
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)

            End Try
        End If
    End Sub
    Private Sub BindEmpGrid()
        lbl_Total.Text = 0
        lbl_counttkt.Text = 0
        Dim dt As New DataTable()
        Try
            Dim objSqlTrans As New SqlTransactionDom
            Dim ds As New DataSet()
            Dim strDate As String = ""
            Dim strEDate As String = ""
              If FromID.Text = "" Then
                strDate = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                strDate = Convert.ToDateTime(FromID.Text.Split("-")(2) + "-" + FromID.Text.Split("-")(1) + "-" + FromID.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 00:00:00.000"
            End If
            If ToID.Text = "" Then
                strEDate = System.DateTime.Now().ToString("yyyy-MM-dd") + " 00:00:00.000"
            Else
                strEDate = Convert.ToDateTime(ToID.Text.Split("-")(2) + "-" + ToID.Text.Split("-")(1) + "-" + ToID.Text.Split("-")(0)).ToString("yyyy-MM-dd") + " 23:59:59.000"
            End If


            ds = objSqlTrans.GetRailBookingReport(Convert.ToString(Session("AgencyID")), txt_OrderId.Text, strDate, strEDate, txt_PNR.Text)
            If ds IsNot Nothing Then
                dt = ds.Tables(0)
            End If
            If dt.Rows.Count > 0 Then

                Dim Amount As Integer
                Amount = dt.Compute("SUM(NETFARE)", "")

                Dim Count As Object
                Count = dt.Rows.Count

                lbl_Total.Text = Amount
                lbl_counttkt.Text = Count
                GridView1.DataSource = dt
                GridView1.DataBind()
            Else
                GridView1.DataSource = Nothing
                GridView1.DataBind()
            End If
        Catch ex As Exception
            Response.Write("Error Occured: " & ex.ToString())
        Finally
            dt.Clear()
            dt.Dispose()
        End Try
    End Sub


    Protected Sub BTN_RAIL_Click(sender As Object, e As EventArgs) Handles BTN_RAIL.Click
        Try
            BindEmpGrid()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
End Class
