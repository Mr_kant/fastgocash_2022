﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="false" CodeFile="RailBookingReport.aspx.vb" Inherits="Report_RailBookingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>
    <div class="container" style="margin-left: -136px;">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-wrapperss">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Rail > Rail Booking Report</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">From Date</label>
                                            <asp:TextBox ID="FromID" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">To Date</label>
                                            <asp:TextBox ID="ToID" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">PNR</label>
                                            <asp:TextBox ID="txt_PNR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Transaction ID</label>
                                            <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pull-right">
                                        <div class="w40 lft">

                                            <asp:Button ID="BTN_RAIL" runat="server" Text="Search" CssClass="button buttonBlue" />
                                        </div>
                                        <div class="w20 lft">&nbsp;</div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        Total Ticket :
                                    <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-md-3">
                                        Total Ticket Amount :
                                    <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                            CssClass="table">
                                            <Columns>

                                                <asp:BoundField DataField="TRANSACTION_ID" HeaderText="TRANSACTION ID" />
                                                <asp:BoundField DataField="PNR_NO" HeaderText="PNR" />
                                                <asp:BoundField DataField="CLASS" HeaderText="CLASS" />
                                                <asp:BoundField DataField="BOOKINGDATE" HeaderText="BOOKING DATE" />

                                                <asp:BoundField DataField="NETFARE" HeaderText="NET FARE" />


                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="col-md-9">
                                        <span style="color: #FF0000">* N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>

